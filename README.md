# Push notification

> Sei italiano o parli italiano? Stai commettendo un errore ENORME: leggi il [README.md](https://gitlab.com/alessandro.battaglia/push-notification/-/blob/main/README-IT.md) in italiano per sistemare le cose.

Or... yust read me in english.

## Installation

1. Clone the repository in you directory. 
    ```sh
    git clone git@gitlab.com:alessandro.battaglia/push-notification.git
    ```

2. Install things (if you use docker, a docker-compose.yml is ready for you, just scroll this page)
    ```sh
    cd push-notification
    npm install
    ```

3. Create keys and insert in code
    ```sh
    ./node_modules/.bin/web-push generate-vapid-keys
    ```
    Replace `<public>` and `<private>` in `server.js` and in `client/client.js` with generated keys.


4. Run the server
    ```sh
    node server.js
    ```

5. Use me
    Double click in client/index.html to use client.

## Use node in docker

1. Create your docker compose:
    ```yaml
    version: "3.2"
    services:
      node:
        restart: always
        image: node
        command: "tail -f /dev/null"
        working_dir: /app
        volumes:
        - .:/app
        ports:
        - "3000:3000"
    ```

2. Execute docker-compose:
    ```sh
    docker-compose up -d
    docker-compose exec node bash
    ```

3. Continue from point 2.
