const publicVapidKey = '<public>';

function urlBase64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

// Check if the serveice worker can work in the current browser
if ('serviceWorker' in navigator) {
    subscribe().catch(err => console.error(err));
}

// Register the service worker, register our push api, send the notification
async function subscribe() {
    // Register service worker
    const register = await navigator.serviceWorker.register('/worker.js', {
        scope: '/'
    });

    // Register push
    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,

        // Public vapid key
        applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
    });

    // Send push notification
    await fetch("http://localhost:3001/subscribe", {
        method: "POST",
        body: JSON.stringify(subscription),
        headers: {
            "content-type": "application/json"
        },
    });
}
