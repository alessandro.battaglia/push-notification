# Push notification

## Installazione

1. Clona la repository nella tua cartella. 
    ```sh
    git clone git@gitlab.com:alessandro.battaglia/push-notification.git
    ```

2. Installa cose (se usi docker, scorri la pagina e avrai tutto già pronto)
    ```sh
    cd push-notification
    npm install
    ```

3. Crea le chiavi e inseriscile nel codice
    ```sh
    ./node_modules/.bin/web-push generate-vapid-keys
    ```
    Sostiruisci `<public>` e `<private>` in `server.js` e in `client/client.js` con le chiavi generate.


4. Avvia il server
    ```sh
    node server.js
    ```

5. Usa il software
    Fai doppio click su client/index.html to use client.

## Usare node in docker

1. Crea il tuo docker compose:
    ```yaml
    version: "3.2"
    services:
      node:
        restart: always
        image: node
        command: "tail -f /dev/null"
        working_dir: /app
        volumes:
        - .:/app
        ports:
        - "3000:3000"
    ```

2. Esegui docker-compose:
    ```sh
    docker-compose up -d
    docker-compose exec node bash
    ```

3. Continua dal punto 2.
