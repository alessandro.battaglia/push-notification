// Guide: https://www.section.io/engineering-education/push-notification-in-nodejs-using-service-worker/

// web-push
const webpush = require('web-push');

// Set storage of subscriptions
let subscriptions = new class extends Map {
    constructor(...props) {
        super(...props);

        // Storing the keys in variables
        // To generate: ./node_modules/.bin/web-push generate-vapid-keys
        const publicVapidKey = '<public>';
        const privateVapidKey = '<private>';

        // Setting vapid keys details
        webpush.setVapidDetails('mailto:<mail>', publicVapidKey, privateVapidKey);
    }

    set(key, value) {
        super.set(key, value);

        // TODO: Add to database
    }

    send(payload, recipient) {
        let recipients = recipient ? [recipient] : this;

        recipients
            .forEach(recipient => {
                // Pass the object into sendNotification fucntion and catch any error
                webpush
                    .sendNotification(recipient, JSON.stringify(payload))
                    .catch(err => console.error(err));
            });
    }
}();

// Express
const express = require('express');

// body-parser
const bodyParser = require('body-parser');

// Using express 
const app = express();

app
    // Using bodyparser
    .use(bodyParser.json())

    // Send additional headers
    .use((req, res, next) => {
        res.append('Response-Type', 'application/json');
        res.append('Access-Control-Allow-Origin', '*');
        res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.append('Access-Control-Allow-Headers', 'Content-Type');
        next();
    })

    // State route
    .post('/state', (req, res) => {
        // Send status 201 for the request
        res.status(200)
            .json({
                success: true,
            });
    })

    // Subscription route (for debug)
    .post('/subscribe', (req, res) => {
        console.log(JSON.stringify(req.body));

        // Send status 200 for the request
        res.status(200)
            .json({
                success: true,
            });
    })

    /**
     * Send notification
     * 
     * POST HTTP/1.0
     * Content-Type: application/json
     * Body: {"titolo":"<Titolo notifica>","messaggio":"<Messaggio notifica>","destinatari":[<...>, <...>]}
     */
    .post('/send', (req, res) => {
        let data = req.body;

        let notification = {
            title: data.titolo,
            body: data.messaggio,
        };

        let recipients = data.destinatari;

        recipients
            .forEach(recipient => {
                webpush
                    .sendNotification(recipient, JSON.stringify(notification))
                    .catch(err => console.error(err.message));
                    // .catch(err => errors.push(err));
            });

        // Send status 200 for the request
        res.status(200)
            .json({
                success: true,
            });
    })

    // Start server
    .listen(3000, () => {
        console.log(`Notification server started on port 3000`);
    });